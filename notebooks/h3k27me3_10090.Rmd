---
title: "h3k27me3 Mouse (10090) Analysis"
output: html_notebook
---

# Load packages
```{r load-libraries}
source('~/git/eye_epigenome/src/GO_enrichment.R')
library(RSQLite)
library(data.table)
library(tidyverse)
library(DESeq2)
library(ggrepel)
library(ggjoy)
source('~/git/scripts/theme_Publication.R')
source('~/git/eye_epigenome/src/peak_annotation.R')
```

# Load counts data and write column names for macs2 gapped peaks (counts for the extra columns)
Only keep autosomes (chr1 - chr19)
```{r read-peak-data}
samples <- c('SRR2132091','SRR2132100','SRR2132097','SRR2132094','SRR2132085','SRR2132088','SRR2721966','SRR2721965','SRR2759040','SRR2759042','SRR2759044','SRR2759045','SRR2759037','SRR2759039','SRR2759038','SRR2759041','SRR3591314','SRR3591320','SRR3591317','SRR4252735','SRR4252729','SRR4252736','SRR4252731','SRR4252755','SRR4252753','SRR4252727','SRR4252733','SRR4252754','SRR4252742','SRR4252743','SRR4252746','SRR4252730','SRR4252741','SRR4252745','SRR4252738','SRR4252737','SRR4252744','SRR4252739','SRR4252747','SRR4252740','SRR4252726','SRR4252732','SRR4252734','SRR4252728')
count_data <- fread('~/git/eye_epigenome/data/peak_counts/h3k27me3_10090_peaks.broadPeak.counts')
colnames(count_data) <- c('seqnames', 'start', 'end', 'ID', 'Strand' ,'Score', 'FoldChange', '-log10pvalue' ,'-log10qvalue', samples)
count_data <- count_data %>% filter(!grepl('GL|X|Y|JH', seqnames))
```

# Pull in metadata, prep colData for deseq, load into DESeq with faux equation
```{r load-metadata}
load('../data/chip_metadata.Rdata')
load('../data/full_chip_metadata.Rdata')
# only keep peaks with a foldchange > 2 over background
count_data_2fc <-count_data %>% filter(FoldChange>2)
count_mat <- count_data_2fc %>%  dplyr::select(SRR2132091:SRR4252728) %>% as.matrix()
row.names(count_mat) <- paste(count_data_2fc$seqnames, as.character(count_data_2fc$start), as.character(count_data_2fc$end), sep ='_')
count_mat %>% head()
chip_metadata %>% filter(`Experiment Run Accession` %in% colnames(count_mat))
coldata <- colnames(count_mat) %>% data.frame()
colnames(coldata) <- c('Experiment Run Accession')
coldata <- left_join(coldata, chip_metadata)
row.names(coldata) <- coldata$`Experiment Run Accession`

dds <- DESeqDataSetFromMatrix(countData = count_mat,
                              colData = coldata,
                              design = ~ 1)
dds
```

# PCA
Age not a huge criteria in outliers
But pure cells versus bulk tissue is. The iPSC retina samples and the rod-specific (flow sorted?) samples are clearly different. The 'wt' cells seem similar enough to keep for now.

```{r pca-1, fig.height=3,fig.width=2.5}
vsd1 <- vst(dds, blind=FALSE)
plotPCA(vsd1, c('Age')) + theme_Publication() + guides(colour=guide_legend(nrow=6,byrow=TRUE)) 
plotPCA(vsd1, c('saCell Type.x')) +  theme_Publication() + guides(colour=guide_legend(nrow=6,byrow=TRUE))
```

# Reset up DESeq data, removing some outliers
```{r filter-metadata}
# also removing any MDMX samples, as they are retinoblastomas
coldata <- coldata %>% 
  filter(!grepl('ipscs',`saCell Type.x`)) %>% filter(!grepl('MDMX', `Sample Attribute`)) %>% left_join(full_chip_metadata, by = c('Experiment Run Accession'='run_accession.x'))
count_mat <- count_mat[,coldata$`Experiment Run Accession`]

dds <- DESeqDataSetFromMatrix(countData = count_mat,
                              colData = coldata,
                              design = ~ 1)
dds
```


# PCA again
We now see a  separation between 'early' (E14.5-P8) and 'late' (P8 - Adult)
```{r pca2, fig.height=3,fig.width=2.5}
vsd2 <- vst(dds, blind=FALSE)
plotPCA(vsd2, c('Age')) + theme_Publication() + geom_text_repel(aes(label=Age)) + guides(colour=guide_legend(nrow=6,byrow=TRUE)) 
plotPCA(vsd2, c('study_accession.x')) + theme_Publication() + guides(colour=guide_legend(nrow=6,byrow=TRUE)) 
plotPCA(vsd2, c('saStrain')) + theme_Publication() + guides(colour=guide_legend(nrow=6,byrow=TRUE))
plotPCA(vsd2, c('Experiment Run Accession')) + theme_Publication() + geom_text_repel(aes(label=Experiment.Run.Accession)) + guides(colour=guide_legend(nrow=2,byrow=TRUE))
```


# Modify coldata again with 'Early' / 'Late' Age and make results
Now give AgeGroup and study_accession 
```{r build-actual-dds}
coldata <- coldata %>% mutate(AgeGroup = ifelse(Age %in% c('E14.5','E17.5','P0','P2','P3','P5','P7', 'P8'), 'Early', 'Late'))

coldata$saStrain[is.na(coldata$saStrain)] <- 'unknown'
dds <- DESeqDataSetFromMatrix(countData = count_mat,
                              colData = coldata,
                              design = ~saStrain + AgeGroup)
dds

dds <- DESeq(dds)
res <- results(dds)
plotMA(res, alpha=1e-4, ylim=c(-5,5))
summary(res, alpha = 0.01)

passed <- coldata %>% dplyr::select(`Experiment Run Accession`, Ab.x) %>% mutate(Ab=Ab.x) %>% dplyr::select(`Experiment Run Accession`, Ab)
write_tsv(passed,'~/git/eye_epigenome/data/samples_passed_QC.dat', append=T)
```

# Print samtools merge command to make merged bam
Then use deeptools to turn it into bigwig
```{r samtools-merge}
call <- coldata %>% group_by(AgeGroup) %>% summarise(samples=paste0(paste0(`Experiment Run Accession`,'.rmdup.ss.bam'),collapse = ' ')) %>% mutate(call=paste0('samtools merge h3k27me3_10090_', AgeGroup, '.ss.rmdup.bam ', samples)) %>% pull(call)
write(call, file = '../src/h3k27me3_10090_merge_bams.swarm')
```

# Turn DESeq2 data into GRanges object for annotating
```{r deseq-to-granges}
h3k27me3_peak_ranges <- deseq2_to_granges(res)
```

# Annotate
```{r annotatr}
h3k27me3_anno <- annotator(h3k27me3_peak_ranges)
h3k27me3_fAnno <- h3k27me3_anno[[1]]
h3k27me3_Anno_counts <- h3k27me3_anno[[2]]
annotations <- h3k27me3_anno[[3]]
tss <- h3k27me3_anno[[4]]
```

# Evaluate mean coverage (peak strength)
```{r mean-peak-coverage}
plot(density(log2(h3k27me3_fAnno$baseMean)))
```

# Make random peaks
```{r random-peaks}
h3k27me3_random_anno_count <- random_peaks(h3k27me3_peak_ranges, annotations)
```

# Integrate random peaks with real peaks
Super high promoter enrichment
```{r real-and-fake-peaks}
all <- h3k27me3_Anno_counts %>% group_by(Category) %>% summarise(Count=n()) %>% mutate(Ratio = Count/sum(Count))
random <- h3k27me3_random_anno_count %>% group_by(Category) %>% summarise(Count=n()) %>% mutate(Ratio = Count/sum(Count))
all$Peaks <- 'h3k27me3'
random$Peaks <- 'Random'
both <- rbind(all,random)

both %>% mutate(Category = fct_relevel(as.factor(Category), c('Promoter',"5'UTR","3'UTR","Exon","Intron","Intergenic"))) %>%  
                  ggplot(aes(y=Ratio, x=Category, fill=Peaks)) + geom_bar(stat='identity', position='dodge') + theme_Publication() + scale_fill_brewer(palette='Set1') + ggtitle('h3k27me3 Peak Distribution')

```

# Generate scaled counts
To make boxplots for individual peaks evaluated
```{r scaled-counts}
normalizedCounts <- t( t(counts(dds)) / sizeFactors(dds) )
anno_norm_Counts <- normalizedCounts %>% 
  data.frame() %>% 
  rownames_to_column('Peak') %>% 
  gather(Sample, `Scaled Count`, -Peak) %>% 
  separate(Peak, c('seqnames','start','end'), sep ='_') %>% 
  left_join(. , coldata %>% dplyr::select(`Experiment Run Accession`, AgeGroup), by = c('Sample'='Experiment Run Accession'))
anno_norm_Counts$start <- as.numeric(anno_norm_Counts$start)
anno_norm_Counts$end <- as.numeric(anno_norm_Counts$end)
```

# Joy plot of peak coverage by class
No differences
```{r}
left_join(h3k27me3_Anno_counts, anno_norm_Counts) %>%  data.frame() %>% 
  mutate(Category = fct_relevel(as.factor(Category), c('Promoter',"5'UTR","3'UTR","Exon","Intron","Intergenic"))) %>% 
  ggplot(aes(x=log2(`Scaled.Count`+1), y=Category)) + geom_joy() + theme_Publication() + ylab('Peak Category')
```

# Which promoters have differential h3k27me3 peaks?
```{r diff-promoter}
# Diff peaks (either direction)
h3k27me3_fAnno %>% filter(abs(log2FoldChange) > 1, Category=='Promoter') %>% data.frame() %>% arrange(-log2FoldChange) %>% nrow()
h3k27me3_fAnno %>% filter(log2FoldChange > 1, Category=='Promoter') %>% data.frame() %>% arrange(-log2FoldChange) %>% dplyr::select(seqnames:padj, mm10_genes_promoters)
h3k27me3_fAnno %>% filter(log2FoldChange < -1, Category=='Promoter') %>% data.frame() %>% arrange(log2FoldChange) %>% dplyr::select(seqnames:padj, mm10_genes_promoters)
```

# Write peaks for UCSC genome browser
```{r ucsc-peaks}
out <- h3k27me3_fAnno %>% arrange(seqnames, start) %>% mutate(name=paste(seqnames,start,end,sep='_'),
                         score='0',
                         strand = '.',
                         thickStart=start,
                         thickEnd=end,
                         itemRgb=case_when(log2FoldChange > 2 ~ '255,0,0',
                                         log2FoldChange > 1 ~ '155,0,0',
                                         log2FoldChange < -2 ~ '0,255,0',
                                         log2FoldChange < -1 ~ '0,155,0',
                                         TRUE ~ '0,0,0')) %>% 
  dplyr::select(seqnames, start, end, name:itemRgb)
write.table(out, file = '../data/h3k27me3_UCSC_peaks.bed', row.names=F, col.names = F, quote=F, sep='\t')
```

Run on biowulf2:
module load ucsc
bedToBigBed ~/git/eye_epigenome/data/h3k27me3_UCSC_peaks.bed /data/mcgaugheyd/genomes/mm10/mm10.chrom.sizes /data/mcgaugheyd/datashare/eye_epigenome/mm10/h3k27me3_UCSC_peaks.bb
