# merge replicates
# as per https://github.com/ENCODE-DCC/chip-seq-pipeline/blob/master/dnanexus/overlap_peaks/src/overlap_peaks.py
# ENCODE keeps peaks with > 0.5 identity by base pair overlap, using intersectBed
# generate pooled list of peaks, then check each 
