#!/bin/bash
# bwa align
# from https://github.com/ENCODE-DCC/dnase_pipeline/blob/v1.2/dnanexus/dnase-align-bwa-se/resources/usr/bin/dnase_align_bwa_se.sh
ref_id=$1
reads1_fq_gz=$2

module load bwa/0.7.15
module load samtools/1.5 

bwa aln -Y -l 32 -n 0.04 -k 2 -t $SLURM_CPUS_PER_TASK $ref_id $reads1_fq_gz > ${reads1_fq_gz%.fastq.gz}.sai
bwa samse -n 10 $ref_id ${reads1_fq_gz%.fastq.gz}.sai $reads1_fq_gz  > ${reads1_fq_gz%.fastq.gz}.sam
samtools view -Shu ${reads1_fq_gz%.fastq.gz}.sam | samtools sort -@ $SLURM_CPUS_PER_TASK -m 2G -o ${reads1_fq_gz%.fastq.gz}.bam -O BAM -
samtools index ${reads1_fq_gz%.fastq.gz}.bam

rm ${reads1_fq_gz%.fastq.gz}.sai
rm ${reads1_fq_gz%.fastq.gz}.sam
