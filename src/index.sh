#!/bin/bash

module load bwa/0.7.15
bwa index -p GRCh38_no_alt_analysis_0.7.15 -a bwtsw GRCh38_no_alt_analysis_set_GCA_000001405.15.fasta.gz
bwa index -p mm10_no_alt_analysis_set_ENCODE_0.7.15 -a bwtsw mm10_no_alt_analysis_set_ENCODE.fasta
