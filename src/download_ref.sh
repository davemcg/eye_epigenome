# get ENCODE ref files
wget https://www.encodeproject.org/files/GRCh38_no_alt_analysis_set_GCA_000001405.15/@@download/GRCh38_no_alt_analysis_set_GCA_000001405.15.fasta.gz # GRCh38 fasta
wget https://www.encodeproject.org/files/mm10_no_alt_analysis_set_ENCODE/@@download/mm10_no_alt_analysis_set_ENCODE.fasta.gz #mm10 fasta

wget https://www.encodeproject.org/files/ENCFF824ZKD/@@download/ENCFF824ZKD.gtf.gz # gtf gencode v24
wget https://www.encodeproject.org/files/gencode.vM7.annotation/@@download/gencode.vM7.annotation.gtf.gz # mm10 gencode m7

# blacklists
# https://sites.google.com/site/anshulkundaje/projects/blacklists
wget http://mitra.stanford.edu/kundaje/akundaje/release/blacklists/hg38-human/hg38.blacklist.bed.gz
wget http://mitra.stanford.edu/kundaje/akundaje/release/blacklists/mm10-mouse/mm10.blacklist.bed.gz