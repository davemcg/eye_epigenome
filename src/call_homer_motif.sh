#!/bin/bash

peaks_bed=$1
out_dir=$2
module load homer/4.8.2
findMotifsGenome.pl $peaks_bed hg38 /data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/homer/$out_dir -size given -preparsedDir /data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/homer
