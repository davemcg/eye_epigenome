#!/bin/bash

module load ChromHMM/1.12

ChromHMM.sh --memory 20g Reorder -m /data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/ChromHMM/labelmappingfile_10states.txt -o /data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/ChromHMM/stateorderingfile_10states.txt /data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/ChromHMM/model_10states/model_10.txt /data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/ChromHMM/model_10states_reordered

ChromHMM.sh --memory 20g MakeSegmentation /data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/ChromHMM/model_10states_reordered/model_10.txt /data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/ChromHMM/binarizeBam /data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/ChromHMM/model_10states_reordered/

ChromHMM.sh --memory 20g MakeBrowserFiles \
	-m /data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/ChromHMM/labelmappingfile_10states_U.txt \
	-c /data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/ChromHMM/colormappingfile.txt \
	/data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/ChromHMM/model_10states_reordered/retina_Early_10090_10_segments.bed \
	Mouse_Retina_Early_ChromHMM \
	/data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/ChromHMM/model_10states_reordered/retina_Early_10090_10

ChromHMM.sh --memory 20g MakeBrowserFiles \
	-m /data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/ChromHMM/labelmappingfile_10states_U.txt \
	-c /data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/ChromHMM/colormappingfile.txt \
	/data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/ChromHMM/model_10states_reordered/retina_Late_10090_10_segments.bed \
	Mouse_Retina_Late_ChromHMM \
	/data/mcgaugheyd/projects/nei/mcgaughey/eye_epigenome/ChromHMM/model_10states_reordered/retina_Late_10090_10
