#!/bin/bash

bash ~/bin/bedToBigBed.sh retina_Early_10090_10_dense.bed /data/mcgaugheyd/genomes/mm10/mm10.chrom.sizes /data/mcgaugheyd/datashare/eye_epigenome/mm10/retina_Early_10090_10_dense.bb
bash ~/bin/bedToBigBed.sh retina_Late_10090_10_dense.bed /data/mcgaugheyd/genomes/mm10/mm10.chrom.sizes /data/mcgaugheyd/datashare/eye_epigenome/mm10/retina_Late_10090_10_dense.bb
