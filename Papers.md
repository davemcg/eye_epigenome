# Publications with eye epigenetic-seq 
- Kim et al., “Recruitment of Rod Photoreceptors from Short-Wavelength-Sensitive Cones during the Evolution of Nocturnal Vision in Mammals”
- Mo et al., “Epigenomic Landscapes of Retinal Rods and Cones”
- Wilken et al., “DNase I Hypersensitivity Analysis of the Mouse Brain and Retina Identifies Region-Specific Regulatory Elements”
- Hennig, Peng, and Chen, “Transcription Coactivators p300 and CBP Are Necessary for Photoreceptor-Specific Chromatin Organization and Gene Expression”
- Corbo et al., “CRX ChIP-Seq Reveals the Cis-Regulatory Architecture of Mouse Photoreceptors”
- Potier et al., “Mapping Gene Regulatory Networks in Drosophila Eye Development by Large-Scale Transcriptome Perturbations and Motif Inference”
- Marcos et al., “Meis1 Coordinates a Network of Genes Implicated in Eye Development and Microphthalmia”
- Jusiak et al., “Regulation of Drosophila Eye Development by the Transcription Factor Sine Oculis”
- Davie et al., “Discovery of Transcription Factors and Regulatory Regions Driving In Vivo Tumor Development by ATAC-Seq and FAIRE-Seq Open Chromatin Profiling.”
- Aldiri et al., “The Dynamic Epigenetic Landscape of the Retina During Development, Reprogramming, and Tumorigenesis.”
- Samuel et al., “Otx2 ChIP-Seq Reveals Unique and Redundant Functions in the Mature Mouse Retina.”
- Ueki et al., “Transgenic Expression of the Proneural Transcription Factor Ascl1 in Müller Glia Stimulates Retinal Regeneration in Young Mice”
- Popova et al., “Stage and Gene Specific Signatures Defined by Histones H3K4me2 and H3K27me3 Accompany Mammalian Retina Maturation In Vivo.”

# Publications with useful ideas 
- Dickel et al., “Genome-Wide Compendium and Functional Assessment of in Vivo Heart Enhancers”