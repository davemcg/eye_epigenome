# Compilation of all eye ChIP-Seq data to identify conserved regulatory regions

# Stage 1: Identify as many epigenetic-seq experiments as possible
    - [Literature searching](https://gitlab.com/davemcg/eye_epigenome/blob/master/Papers.md)
    - [SRA search](https://gitlab.com/davemcg/eye_epigenome/blob/master/sra_search.R)
    
    
```
JM: How to find distal regulatory elements?
1/Sequence conservation
2/p300 ChIP
3/H3K27ac+H3K4me1
4/regions bound by many TFs
```